require 'roo'

xlsx = Roo::Spreadsheet.open(Rails.root.join('db', 'podcasts.xlsx').to_s)
sheet_spotify = xlsx.sheet('spotify')
(2..sheet_spotify.last_row).each do |i|
  title = sheet_spotify.row(i)[0]
  name = sheet_spotify.row(i)[1]
  custom_url = sheet_spotify.row(i)[2]
  description = sheet_spotify.row(i)[3]

Spotify.create!(name: name,  custom_url: custom_url, description: description, title: title)

end

sheet_apple = xlsx.sheet('apple')
(2..sheet_apple.last_row).each do |i|
  title = sheet_apple.row(i)[0]
 name = sheet_apple.row(i)[1]
 custom_url = sheet_apple.row(i)[2]
 description = sheet_apple.row(i)[3]

Apple.create!(name: name,  custom_url: custom_url, description: description, title: title)

end

#Product.create(name: '600秒歷史課', description:'123', custom_url: 'https://www.rti.org.tw/')
