class CreateSpotifys < ActiveRecord::Migration[7.0]
  def change
    create_table :spotifys do |t|
      t.string :name
      t.text :description
      t.string :custom_url

      t.timestamps
    end
  end
end
