Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  resources :items
  resources :products
  resources :spotifys
  resources :apples
  root 'spotifys#index'
  

  get 'spotify', to: 'spotify#index'
  get 'apple', to: 'apple#index'
end
