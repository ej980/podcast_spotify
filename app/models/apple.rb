class Apple < ApplicationRecord
  #validates :custom_url, presence: true, uniqueness: true
  #validates :name, presence: true
  #validates :custom_url, presence: true, uniqueness: true
  #default_scope -> { order(created_at: :asc) }
  self.table_name = 'apples'
  attribute :custom_url, :string
  def external_link
    "#{custom_url}"
  end
end
