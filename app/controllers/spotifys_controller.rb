class SpotifysController < ApplicationController

  def index
    @spotifys = Spotify.all
    @spotifys = Spotify.order(:title)
    #render 'index'
    #@products = Product.order(created_at: :desc).all
  end

  def edit
    @spotify = Spotify.find(params[:id])
  end

  def update
    @spotify = Spotify.find(params[:id])

    if @spotify.update(spotify_params)
      redirect_to spotifys_path
      flash.now[:alert] = "success"
    else
      render :edit
    end
  end

  def create
    @spotify = Spotify.new(spotify_params)

    if @spotify.save
      redirect_to spotifys_path, notice: '建立完成'
    else
      render :new
    end
  end

  def destroy
    @spotify = Spotify.find(params[:id])
    @spotify.destroy
        redirect_to spotifys_path, notice: '已刪除'
  end

  def new
    @spotify = Spotify.new
  end

  def show
    @spotify = Spotify.find(params[:id])
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def spotify_params
    params.require(:spotify).permit(:name, :custom_url, :description, :title)
  end

end
