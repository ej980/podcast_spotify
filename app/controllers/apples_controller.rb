class ApplesController < ApplicationController
  def index
    @apples = Apple.all
    @apples = Apple.order(:title)
    #render 'index'
    #@products = Product.order(created_at: :desc).all
  end

  def edit
    @apple = Apple.find(params[:id])
  end

  def update
    @apple = Apple.find(params[:id])

    if @apple.update(apple_params)
      redirect_to apples_path
      flash.now[:alert] = "success"
    else
      render :edit
    end
  end

  def create
    @apple = Apple.new(apple_params)

    if @apple.save
      redirect_to apples_path, notice: '建立完成'
    else
      render :new
    end
  end

  def destroy
    @apple = Apple.find_by(id: params[:id])
    @apple.destroy
        redirect_to apples_path, notice: '已刪除'
  end

  def new
    @apple = Apple.new
  end

  def show
    @apple = Apple.find(params[:id])
  end

  private

  def set_product
    @apple = Apple.find(params[:id])
  end

  def apple_params
    params.require(:apple).permit(:name, :custom_url, :description, :title)
  end

end
