class ProductsController < ApplicationController

  def index
    @products = Product.all
    #@products = Product.order(created_at: :desc).all
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])

    if @product.update(product_params)
      redirect_to products_path
      flash.now[:alert] = "success"
    else
      render :edit
    end
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to products_path, notice: '建立完成'
    else
      render :new
    end
  end

  def destroy
    @product = Product.find_by(id: params[:id])
    @product.destroy
        redirect_to products_path, notice: '已刪除'
  end

  def new
    @product = Product.new
  end

  def show
    @product = Product.find(params[:id])
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :custom_url, :description)
  end

end
